<?php

require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new animal("shaun");
$sheep->get_name();
echo '<br/>';
$sheep->get_legs();
echo '<br/>';
$sheep->get_cold_blooded();
echo '<br/><br/>';

$ape = new ape("kera sakti");
$ape->get_name();
echo '<br/>';
$ape->get_legs();
echo '<br/>';
$ape->get_cold_blooded();
echo '<br/>';
$ape->yell();
echo '<br/><br/>';

$frog = new frog("buduk");
$frog->get_name();
echo '<br/>';
$frog->get_legs();
echo '<br/>';
$frog->get_cold_blooded();
echo '<br/>';
$frog->jump();
