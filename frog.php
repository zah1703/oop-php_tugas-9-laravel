<?php

require_once 'animal.php';

class frog extends animal
{
    public function jump(): void
    {
        echo "jump: hop hop" . PHP_EOL;
    }
}
