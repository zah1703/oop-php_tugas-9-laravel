<?php

class animal
{
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";
    public function __construct($name)
    {
        $this->name = $name;
    }

    public function get_name(): void
    {
        echo "Name: $this->name" . PHP_EOL;
    }

    public function get_legs(): void
    {
        echo "Legs: $this->legs" . PHP_EOL;
    }

    public function get_cold_blooded(): void
    {
        echo "Cold blooded: $this->cold_blooded" . PHP_EOL;
    }
}
